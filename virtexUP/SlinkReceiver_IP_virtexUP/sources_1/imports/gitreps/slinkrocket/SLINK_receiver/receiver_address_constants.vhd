library IEEE;
library WORK;
--use IEEE.std_logic_1164.all;
--use IEEE.std_logic_arith.all;

--use ieee.numeric_std.all;

--this address should be multiply by 4 to be byte aligned
-- the selection chip are 16#0 ...16#3FFF   64 bit addressing

-- the receiver needs currently 8 addresses

package address_table is

--################################################################################################################################
 

        constant        slinkrocket_access_command      : integer := 16#0000#; --x2400
                                                                               -- execute the command (the data should be set first)
                                                                               -- send the offset
                                                                               --read b0 access done
                                                                               --read b1 access good
        constant        slinkrocket_access_data         : integer := 16#0001#; --x2408
                                                                               --write data to write    
                                                                               --read  data received
        constant        slinkrocket_resync_request      : integer := 16#0002#; --x2410
                
        constant        slinkrocket_rcv_serdes_control                  : integer := 16#0003#;  
        constant        slinkrocket_rcv_serdes_loopback                 : integer := 16#0004#;  
        constant        slinkrocket_rcv_serdes_drp_control              : integer := 16#0005#;          
        constant        slinkrocket_rcv_serdes_drp_data_returned        : integer := 16#0006#;  
        constant        slinkrocket_rcv_serdes_status                   : integer := 16#0007#;  
        constant        slinkrocket_rcv_serdes_analog                   : integer := 16#0008#;  
        constant		slinkrocket_serdes_prbs_test					: integer := 16#0009#;
        constant        slinkrocket_fedx_fragment_rcv                   : integer := 16#000A#; 
        constant        slinkrocket_fedx_fragment_rcv_wth_error         : integer := 16#000B#; 
                
end package address_table;
