----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11.03.2019 16:03:38
-- Design Name: 
-- Module Name: GLB_send_rec_sr - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use work.address_table.all;
--use work.receiver_address_constants.all; 
 

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SR_Receive_GLB is 
 generic (  Add_Offset_SLINKR                       : integer := 0;
		    Clock_source							: string := "Master";
				--possible choices are Slave or Master
             throughput								: string := "15.66";
				--possible choices are  15.66 or 25.78125 
			technology								: string := "GTY" ;
            UltraRam_mem                            : boolean := TRUE  
				); --  
                                               -- Interface for the reset handling.
 Port (
		usr_clk							: in std_logic;
		rst_usr_clk_n					: in std_logic;
		usr_func_wr						: in std_logic_vector(16383 downto 0); 
		usr_wen							: in std_logic;
		usr_data_wr						: in std_logic_vector(63 downto 0); 
										
		usr_func_rd						: in std_logic_vector(16383 downto 0);
		usr_rden						: in std_logic;
		usr_dto_receiver				: out std_logic_vector(63 downto 0);
			
			
		clk_freerun_in	   				: in std_logic; --can be a clock between 3.125 Mhz to 125 Mhz
			
		clk_out							: out std_logic;
		wen_data						: out std_logic; -- active high
		UCTRL_out						: out std_logic; -- active High to indicates a Trailer a Header  
		data_out						: out std_logic_vector(127 downto 0);
		lff								: in std_logic; --backpressure active high
			
		ena_cnt_pckt					: out std_logic; 
		ena_cnt_bad_p					: out std_logic; 
		ena_back_pres					: out std_logic; 
			
		qpll_lock_in					: IN STD_LOGIC;
		qpll_reset_out					: OUT STD_LOGIC;
		qpll_clkin						: IN STD_LOGIC;
		qpll_ref_clkin					: IN STD_LOGIC;
		
		--  Clock source and destination
		--Clock control to/from  SLAVE SERDES
		-- These signals are to/from the SLAVE serdes to be used to generate the master clock
		gtS_Reset_TX_clock_out			: out std_logic;                 				-- this signals are displayed for the Slave SERDES 
		gtS_userclk_tx_active_in		: in std_logic := '0';                  		-- this signals are displayed for the Slave SERDES 
		gtS_userclk_tx_usrclk_in		: in std_logic := '0';                  		-- this signals are displayed for the Slave SERDES
		gtS_userclk_tx_usrclk2_in		: in std_logic := '0';                  		-- this signals are displayed for the Slave SERDES
		gtS_userclk_tx_usrclk4_in		: in std_logic := '0';                  		-- this signals are displayed for the Slave SERDES 
		
		--Clock Control to/from MASTER CLOCK LOGIC 
		-- these signals are source to generate the master clock of the serdes
		gtM_Reset_TX_clock_in_0			: in std_logic := '0';--active HIGH -- this signals is displayed for the Master SERDES 
		gtM_Reset_TX_clock_in_1			: in std_logic := '0';--active HIGH -- this signals is displayed for the Master SERDES 
		gtM_Reset_TX_clock_in_2			: in std_logic := '0';--active HIGH -- this signals is displayed for the Master SERDES  
		gtM_userclk_tx_active_out		: out std_logic := '0';                         -- this signals is displayed for the Master SERDES 
		gtM_userclk_tx_usrclk_out		: out std_logic := '0';                         -- this signals is displayed for the Master SERDES
		gtM_userclk_tx_usrclk2_out		: out std_logic := '0';                         -- this signals is displayed for the Master SERDES
		gtM_userclk_tx_usrclk4_out		: out std_logic := '0';                         -- this signals is displayed for the Master SERDES 
				
		Rcv_gt_rxn_in	 				: in std_logic;  	
        Rcv_gt_rxp_in	 				: in std_logic;  	
        Rcv_gt_txn_out	 				: out std_logic; 
        Rcv_gt_txp_out	 				: out std_logic;
        
        SERDES_ready                    : out std_logic         -- inform when the link is Synchronized with the sender part
 );
end SR_Receive_GLB;
	
--*///////////////////////////////////////////////////////////////////////////////
--*////////////////////////   Behavioral        //////////////////////////////////
--*///////////////////////////////////////////////////////////////////////////////
architecture Behavioral of SR_Receive_GLB is

component resetn_resync is
port (
	aresetn				: in std_logic;
	clock				: in std_logic; 
	Resetn_sync			: out std_logic;
	Resetp_sync			: out std_logic
	);
end component;
 
signal wen_data_cell	    : std_logic; -- active high        
signal UCTRL_out_cell		: std_logic; -- active High to ind
signal data_out_cell	    : std_logic_vector(127 downto 0);    
  

signal gtM_userclk_rx_active                        : std_logic;

signal gtM_Clock_Src_TX 							: std_logic; 
signal gtM_Reset_TX_clock 							: std_logic; 
signal gtM_userclk_tx_active 						: std_logic; 
signal gtM_userclk_tx_usrclk 						: std_logic;
signal gtM_userclk_tx_usrclk2 						: std_logic;
signal gtM_userclk_tx_usrclk4 						: std_logic; 

  

signal DAQ_serdes_RX_hd  	            	: std_logic_vector(5 downto 0);		 
signal DAQ_serdes_RX                		: std_logic_vector(127 downto 0);
signal DAQ_serdes_TX_hd                		: std_logic_vector(5 downto 0);	
signal DAQ_serdes_TX                   		: std_logic_vector(127 downto 0);
signal SERDES_ready_cell    		        : std_logic := '1'; 					-- initialization of SERDES is done
   
signal ext_cmd						        : std_logic_vector(15 downto 0) := x"0000"	;   -- generate errors
 	
signal ack_cnt						        : std_logic; -- Ack sent
signal pckt_cnt						        : std_logic; -- good packet received

signal clock_trans_serdes_DAQ				: std_logic;
signal reset_trans_serdes_DAQ				: std_logic;
signal clock_rcv_serdes_DAQ					: std_logic;
signal reset_rcv_serdes_DAQ					: std_logic;

signal user_DTO_rcv							: std_logic_vector(63 downto 0);
signal usr_dto_Serdes_sender				: std_logic_vector(63 downto 0);
signal usr_dto_generator					: std_logic_vector(63 downto 0);

signal link_init					        : std_logic;  
signal prev_acc_end				            : std_logic;
signal CUR_seq_num					        : std_logic_vector(31 downto 0);
signal CMD_seq_num					        : std_logic_vector(31 downto 0);
 
signal usr_dto_receiver_cell				: std_logic_vector(63 downto 0);

signal gtM_Reset_TX_clock_in : std_logic_vector(2 downto 0);
signal gtM_Reset_RX_clock_in : std_logic_vector(2 downto 0);


--#############################################################################
-- Code start here
--#############################################################################
begin

  gtM_Reset_TX_clock_in <= (gtM_Reset_TX_clock_in_0,gtM_Reset_TX_clock_in_1,gtM_Reset_TX_clock_in_2); 
   
reset_resync_i1:resetn_resync 
port map(
	aresetn				=>	rst_usr_clk_n,
	clock				=>	clock_trans_serdes_DAQ,
	Resetn_sync			=>	reset_trans_serdes_DAQ 
	);	
	
reset_resync_i2:resetn_resync 
port map(
	aresetn				=>	rst_usr_clk_n,
	clock				=>	clock_rcv_serdes_DAQ,
	Resetn_sync			=>	reset_rcv_serdes_DAQ 
	);			
 
Clock_master_SERDES:if Clock_source = "Master" generate
	-- instantiation of the serdes for the sender MASTER MODE 
	serdes_receiver_i1:entity work.Serdes_wrapper_rec_inst  
	 generic map (SR_Serdes_offset  		=> Add_Offset_SLINKR, 
				  Clock_source				=> "Master"			,
				  throughput				=> throughput,  
				  technology				=> technology 
				)
	  Port map(
		-- PCIe local interface
			usr_clk							=> usr_clk			                  , 		    
			rst_usr_clk_n					=> rst_usr_clk_n	                  , 		    
			usr_func_wr						=> usr_func_wr		                  , 		 
			usr_wen							=> usr_wen			                  , 		    
			usr_data_wr						=> usr_data_wr		                  , 		
																				    
			usr_func_rd						=> usr_func_rd		                  , 		 
			usr_rden						=> usr_rden			                  , 			
			usr_dto							=> usr_dto_Serdes_sender			  , 		
											
	  -- data bus                          
			userclk_tx_srcclk_out			=> clock_trans_serdes_DAQ             ,--		    -- FRAME to send over SERDES
			tx_header						=> DAQ_serdes_TX_hd                   ,--			-- bit control of the 64/66 encoding
			tx_data							=> DAQ_serdes_TX                      ,--			-- data word
											
			userclk_rx_srcclk_out			=> clock_rcv_serdes_DAQ               ,--		    -- FRAME received over SERDES
			-- rx_data_valid					=>                                ,--			-- valid data word
			rx_header						=> DAQ_serdes_RX_hd                   ,--			-- header bit (64/66 encoding)
			-- rx_header_valid					=>                                ,--		    -- valid header bits
			rx_data							=> DAQ_serdes_RX               		  ,--			-- data words (2 x 64 bit)
										 
			SERDES_ready					=> SERDES_ready_cell                       ,           
	  --   Gb serdes interface              
			clk_freerun_in					=> clk_freerun_in                    ,--			-- reference clocks QPLL signals
		
	 --  Clock source and destination
			--Clock control to/from  SERDES/logic
			-- These signals are from the serdes to be used to generate the master clock
			gtM_Clock_Src_TX_out			=> gtM_Clock_Src_TX 		          , 
			gtx_Reset_TX_clock_out			=> gtM_Reset_TX_clock 	              ,  
			gtx_userclk_tx_active_in		=> gtM_userclk_tx_active              , 
			gtx_userclk_tx_usrclk_in		=> gtM_userclk_tx_usrclk              ,
			gtx_userclk_tx_usrclk2_in		=> gtM_userclk_tx_usrclk2             ,
			gtx_userclk_tx_usrclk4_in		=> gtM_userclk_tx_usrclk4             , 
			
			--Clock Control to/from MASTER 
			-- these signals are source to generate the master clock of the serdes
			gtM_Clock_Src_TX_in				=> gtM_Clock_Src_TX 	              , 
			gtM_Reset_TX_clock_in(0)		=> gtM_Reset_TX_clock                 ,
			gtM_Reset_TX_clock_in(3 downto 1)=> gtM_Reset_TX_clock_in             , 
			gtM_userclk_tx_active_out		=> gtM_userclk_tx_active              , 
		    gtM_userclk_rx_active_out		=> gtM_userclk_rx_active                 , 
			gtM_userclk_tx_usrclk_out		=> gtM_userclk_tx_usrclk              ,
			gtM_userclk_tx_usrclk2_out		=> gtM_userclk_tx_usrclk2             ,
			gtM_userclk_tx_usrclk4_out		=> gtM_userclk_tx_usrclk4             , 
	 -- QPLL 				                                
			qpll_lock_in					=> qpll_lock_in			              ,--			
			qpll_reset_out					=> qpll_reset_out		              ,--			
			qpll_clk_in						=> qpll_clkin			              ,--			
			qpll_refclk_in					=> qpll_ref_clkin		              ,--			
	 -- High speed link                                      
			gt_rxn_in(0)                    => Rcv_gt_rxn_in                      ,--   	  	-- SERDES connection 
			gt_rxp_in(0)                    => Rcv_gt_rxp_in                      ,--   	  	
			gt_txn_out(0)                   => Rcv_gt_txn_out                     ,--     	
			gt_txp_out(0)                   => Rcv_gt_txp_out --     	            		
	  );
	   
	gtM_userclk_tx_active_out				<= gtM_userclk_tx_active;  
	
	gtM_userclk_tx_usrclk_out				<= gtM_userclk_tx_usrclk; 
	gtM_userclk_tx_usrclk2_out				<= gtM_userclk_tx_usrclk2; 
	gtM_userclk_tx_usrclk4_out				<= gtM_userclk_tx_usrclk4; 
	 
end generate;

Clock_slave_SERDES:if Clock_source = "Slave" generate
	-- instantiation of the serdes for the sender SLAVE MODE
	serdes_receiver_i1:entity work.Serdes_wrapper_rec_inst  
	 generic map (SR_Serdes_offset  		=> Add_Offset_SLINKR, 
				  Clock_source				=> "Slave"			,
				  throughput				=> throughput,  
				  technology				=> technology 
				)
	  Port map(
		-- PCIe local interface
			usr_clk							=> usr_clk			                ,--			    
			rst_usr_clk_n					=> rst_usr_clk_n	                ,--			    
			usr_func_wr						=> usr_func_wr		                ,--			 
			usr_wen							=> usr_wen			                ,--			    
			usr_data_wr						=> usr_data_wr		                ,--			
															
			usr_func_rd						=> usr_func_rd		                ,--			 
			usr_rden						=> usr_rden			                ,--				
			usr_dto							=> usr_dto_Serdes_sender			,--			
											
	  -- data bus                          
			userclk_tx_srcclk_out			=> clock_trans_serdes_DAQ           ,--		    -- FRAME to send over SERDES
			tx_header						=> DAQ_serdes_TX_hd                 ,--			-- bit control of the 64/66 encoding
			tx_data							=> DAQ_serdes_TX                    ,--			-- data word
											
			userclk_rx_srcclk_out			=> clock_rcv_serdes_DAQ             ,--		    -- FRAME received over SERDES
			-- rx_data_valid					=>                              ,--			-- valid data word
			rx_header						=> DAQ_serdes_RX_hd                 ,--			-- header bit (64/66 encoding)
			-- rx_header_valid					=>                              ,--		    -- valid header bits
			rx_data							=> DAQ_serdes_RX               		,--			-- data words (2 x 64 bit)
										 
			SERDES_ready					=> SERDES_ready_cell                     ,           
	  --   Gb serdes interface              
			clk_freerun_in					=> clk_freerun_in                  ,--			-- reference clocks QPLL signals
		
	 --  Clock source and destination
			--Clock control to/from  SERDES/logic
			-- These signals are from the serdes to be used to generate the master clock
			gtx_Reset_TX_clock_out			=> gtS_Reset_TX_clock_out 	        ,  
			gtx_userclk_tx_active_in		=> gtS_userclk_tx_active_in         , 
			gtx_userclk_tx_usrclk_in		=> gtS_userclk_tx_usrclk_in         ,
			gtx_userclk_tx_usrclk2_in		=> gtS_userclk_tx_usrclk2_in        ,
			gtx_userclk_tx_usrclk4_in		=> gtS_userclk_tx_usrclk4_in        , 
			
			--Clock Control to/from MASTER 
			-- these signals are source to generate the master clock of the serdes
			gtM_Clock_Src_TX_in				=> '0' 	                            , 
			gtM_Reset_TX_clock_in => "0000"                           , 
		    gtM_userclk_rx_active_out		=> gtM_userclk_rx_active                 , 
	 -- QPLL 				                                
			qpll_lock_in					=> qpll_lock_in			            ,--			
			qpll_reset_out					=> qpll_reset_out		            ,--			
			qpll_clk_in						=> qpll_clkin			            ,--			
			qpll_refclk_in					=> qpll_ref_clkin		            ,--			
	 -- High speed link                                      
			gt_rxn_in(0)                    => Rcv_gt_rxn_in                    ,--   	  	-- SERDES connection 
			gt_rxp_in(0)                    => Rcv_gt_rxp_in                    ,--   	  	
			gt_txn_out(0)                   => Rcv_gt_txn_out                   ,--     	
			gt_txp_out(0)                   => Rcv_gt_txp_out --     	            		
	  );

end generate;	
  
SERDES_ready    <= SERDES_ready_cell;  
--###############################	 
--Receiver part DAQ
	 	 	
 	
Receiver_core_i1:entity work.Slink_opt_rcv 
generic map(Add_Offset_SLINKR		=> Add_Offset_SLINKR,
            UltraRam_mem            => UltraRam_mem ) 
port map(
	resetn_r					=>	reset_rcv_serdes_DAQ		            ,
	clock_r						=>	clock_rcv_serdes_DAQ		            ,
	resetn_t					=>	reset_trans_serdes_DAQ		            ,
	clock_t						=>	clock_trans_serdes_DAQ		            ,
		
	--FED block coming out		
	Evt_rd_data					=>	wen_data_cell			            	,-- active high
	Evt_UCTRL_out				=>	UCTRL_out_cell		                    ,-- active High to indicates a Trailer or a Header   
	Evt_data_out				=>	data_out_cell		               	 	,
	Evt_lff						=>	lff        						        ,--backpressure active low

	--	bus from serdes					
	Init_serdes					=>	SERDES_ready_cell   		            ,-- initialization of SERDES is done
	DAQ_serdes_RX_hd  	        =>	DAQ_serdes_RX_hd,	 
	DAQ_serdes_RX               =>	DAQ_serdes_RX,
                                 
	-- bus to serdes							   
	ext_cmd						=>	ext_cmd, -- generate errors
	DAQ_serdes_TX_hd            =>	DAQ_serdes_TX_hd,
	DAQ_serdes_TX               =>	DAQ_serdes_TX,
 
	ack_cnt						=>	ack_cnt				                    ,-- Ack sent
	pckt_cnt					=>	pckt_cnt			                    ,-- good packet received
	--control bus					  
	USR_resetn					=>	rst_usr_clk_n				            ,
	USR_clk						=>	usr_clk			                        ,
	USR_wr_func					=> 	usr_func_wr					            ,
	USR_rd_func					=> 	usr_func_rd					            , 
	USR_dti						=>	usr_data_wr			                	,
	USR_wen						=>	usr_wen			                        , 
	USR_dto 					=>	user_DTO_rcv		                    , 
	link_init					=>	link_init			                    ,-- set to '1' after reception of init_link
	prev_acc_end				=>	prev_acc_end		                    ,
	CUR_seq_num					=>	CUR_seq_num			                    ,
	CMD_seq_num					=>	CMD_seq_num			                    ,
	cnt_pckt					=>	ena_cnt_pckt			                ,-- total packet received
	cnt_bad_p					=>	ena_cnt_bad_p			                , -- bad packet received
	back_pres					=>	ena_back_pres			                  
);	


wen_data	           <= wen_data_cell;     
UCTRL_out	           <= UCTRL_out_cell;      
data_out	           <= data_out_cell;      
     
clk_out				   <= clock_rcv_serdes_DAQ;
 
    
usr_dto_receiver <= user_DTO_rcv or usr_dto_Serdes_sender;

end Behavioral;
