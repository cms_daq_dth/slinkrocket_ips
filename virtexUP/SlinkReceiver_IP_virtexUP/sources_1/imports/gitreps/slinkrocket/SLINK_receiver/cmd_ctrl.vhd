
------------------------------------------------------
-- Manage COMMAND_number from PCIe to SLINK opt
--
--  Ver 2.00
--
-- Dominique Gigi March 2012
------------------------------------------------------
--   change the PCIe output data
--  
-- 
--  
------------------------------------------------------
LIBRARY ieee;
 
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use work.address_table.all;
--use work.receiver_address_constants.all;
 
entity cmd_ctrl is
generic ( timer_out : std_logic_vector(15 downto 0):=x"0800";
		address_offset		: integer := 0	);
port (
	USR_resetn		: in std_logic;
	USR_clk			: in std_logic;
	USR_wr_func		: in std_logic_vector(16383 downto 0);--func(0) set the address (bit 31 = 1 write =0 read, func(1) set the data 
	USR_rd_func		: in std_logic_vector(16383 downto 0);--func(0) set the address (bit 31 = 1 write =0 read, func(1) set the data 
	USR_dti			: in std_logic_vector(63 downto 0);
	USR_wen			: in std_logic;	
	USR_dto 	    : out std_logic_vector(63 downto 0);
	access_end	    : out std_logic;
	
	Grst_clk_TX	    : in std_logic;
	clk_TX		    : in std_logic;
	COMMAND_req		: out std_logic;
	COMMAND_number	: out std_logic_vector(63 downto 0);
	COMMAND_data	: out std_logic_vector(63 downto 0);
	COMMAND_Seq_num	: out std_logic_vector(31 downto 0);
	
	Grst_clk_RX	    : in std_logic;
	clk_RX		    : in std_logic;
	init_link	    : in std_logic; -- reset the seq to 0x0000001
	link_init	    : out std_logic; -- set to '1' after reception of init_link
	ACK_rcv		    : in std_logic;
	ACK_data_i		: in std_logic_vector(63 downto 0);
	ACK_seq_num_i	: in std_logic_vector(31 downto 0)


);
end cmd_ctrl;

architecture behavioral of cmd_ctrl is
 

signal Cmd_OL				: std_logic_vector(63 downto 0);
signal rd_req				: std_logic;
signal wr_req				: std_logic;
signal data_ol				: std_logic_vector(63 downto 0);
signal seq_num				: std_logic_vector(31 downto 0);
signal seq_num_ck			: std_logic_vector(31 downto 0);
signal inc_seqnm			: std_logic;
signal resync_init		    : std_logic;
--signal resync_ack			: std_logic;
signal resync_rdrq		    : std_logic;
signal resync_wrrq		    : std_logic;
signal seq_num_rcv		    : std_logic_vector(31 downto 0);
signal data_rcv			    : std_logic_vector(63 downto 0);
signal data_tmp			    : std_logic_vector(63 downto 0);
signal dt_valid			    : std_logic_vector(2 downto 0);
signal dt_valid_sync_user   : std_logic;
signal time_out			    : std_logic_vector(15 downto 0);
signal set_TO				: std_logic;
signal rq_retrans			: std_logic;
signal req_resync			: std_logic;
signal sync_req_resync	    : std_logic;
signal USR_dt_rdy		    : std_logic;
signal access_end_reg	    : std_logic;

signal link_init_done	    : std_logic;
signal compare_seq_num		: std_logic;
signal load_new_seq_n		: std_logic;
signal load_new_seq_n_resync		: std_logic;

signal USR_dto_rg	 	    : std_logic_vector(63 downto 0);

--attribute mark_debug : string;
--attribute mark_debug of USR_dto_rg	: signal is "true"; 
--attribute mark_debug of Cmd_OL		: signal is "true"; 
--attribute mark_debug of data_ol		: signal is "true"; 
--attribute mark_debug of rd_req		: signal is "true"; 
--attribute mark_debug of wr_req		: signal is "true"; 

--**************************************************************************************************
--****************************************<<         BEGIN          >>******************************
--**************************************************************************************************
begin

resync_pulse_seq_n_load:entity work.resync_pulse 
port map(
	aresetn			=> '1',
	clocki			=> clk_TX,
	in_s			=> load_new_seq_n,
	clocko			=> clk_RX,
	out_s			=> load_new_seq_n_resync
	);

process(clk_RX)
begin
	if rising_edge(clk_RX) then
		--compare sequence number sent (request) and received (answer)
		compare_seq_num		<= '0';
		if seq_num_rcv = seq_num_ck then
			compare_seq_num	<= '1';
		end if;	
		
		if load_new_seq_n_resync = '1' then
		    seq_num_ck 			<= seq_num;
		end if;
		 
		-- stores Sequence number and data received from an Acknowledge
		if ack_rcv = '1' then
			seq_num_rcv 	<= ACK_seq_num_i;
			data_tmp 		<= ACK_data_i; -- memorise the data /status received 
		end if;
	end if;
end process;

-- memorize the initialization DONE
process(Grst_clk_RX,clk_RX)
begin
	if Grst_clk_RX = '0' then
		link_init_done 		<= '0';
	elsif rising_edge(clk_RX) then
		if init_link = '1' then
			link_init_done 	<= '1';
		end if;
	end if;
end process;

-- delay pulse acknowledge received to have time to check the sequence number match
process(Grst_clk_RX,clk_RX)
begin
	if Grst_clk_RX = '0' then
		dt_valid(2 downto 0)<= (others => '0');
		data_rcv			<= (others => '0');
	elsif rising_edge(clk_RX) then
		dt_valid(2) 		<= '0';
		if dt_valid(1) = '1' AND compare_seq_num = '1' then
			data_rcv 		<= data_tmp;	-- transfer the Data/status received only if it is the correct Seq_number
			dt_valid(2) 	<= '1';
		end if;
		
		--validate the data if the sequence number sent and received matched (CRC was already checked)
		 
		dt_valid(1) <= dt_valid(0);
		
		dt_valid(0) <= '0';
		if ack_rcv = '1' then 
			dt_valid(0)		<= '1';
		end if;
	
	end if;
end process;

link_init		<= link_init_done;

-- resync between clock domains
resync_pulse_SXclk_SXclk:entity work.resync_pulse 
port map (
	aresetn			=> Grst_clk_RX,
	clocki			=> clk_RX,
	in_s			=> init_link,
	clocko			=> clk_TX,
	out_s 			=> resync_init
	);

resync_pulse_SXclk_pci:entity work.resync_pulse 
port map (
	aresetn			=> Grst_clk_RX,
	clocki			=> clk_RX,
	in_s			=> dt_valid(2),
	clocko			=> USR_clk,
	out_s 			=> dt_valid_sync_user
	);
	
resync_pulse_SXclk_SXclk_i1:entity work.resync_pulse 
port map (
	aresetn			=> Grst_clk_RX,
	clocki			=> clk_RX,
	in_s			=> dt_valid(2),
	clocko			=> clk_TX,
	out_s 			=> inc_seqnm
	);	
	
resync_pulse_pci_SXclk_i1:entity work.resync_pulse 
port map (
	aresetn			=> USR_resetn,
	clocki			=> USR_clk,
	in_s			=> req_resync,
	clocko			=> clk_TX,
	out_s 			=> sync_req_resync
	);	
	
--seq_num management for COMMAND_number going over the SR  (DAQ -> FED)
process(Grst_clk_TX,clk_TX)
begin
	if Grst_clk_TX = '0' then
		seq_num 			<= (others => '0');
		load_new_seq_n      <= '0';
	elsif rising_edge(clk_TX) then
	   
		load_new_seq_n      <= '0';
		--sync request reset the sequence number
		if sync_req_resync = '1' then
		    seq_num 		<= x"00000000";
		    load_new_seq_n    <= '1';
		-- resync Init set sequencer number to 1
		elsif resync_init = '1' then
	        seq_num 		<= x"00000001";
		    load_new_seq_n    <= '1';
		-- increment by 1 sequence number if packet is validated
		elsif inc_seqnm = '1' then
			if seq_num = x"7FFFFFFF" then	-- seq number use only 31 bit bit 32 specify if it is an ack or no
				seq_num 	<= x"00000001";
			else
				seq_num 	<= seq_num + "1";
			end if;
			load_new_seq_n    <= '1';
		end if;
	end if;
end process;

	
	
--PCI decoding
process(USR_resetn,USR_clk)
begin
	if USR_resetn = '0' then
		rd_req 				<= '0';
		wr_req				<= '0';
		access_end_reg 		<= '1';
		req_resync 			<= '0';
		USR_dt_rdy 		<= '0';
	elsif rising_edge(USR_clk) then
		rd_req 				<= '0';
		wr_req 				<= '0';
		req_resync 			<= '0';
		if USR_wen = '1' then
			if      USR_wr_func(address_offset +	Slinkrocket_access_command) = '1'  then
				if USR_dti(31) = '0' then -- read request (bit 31 = '1' write request)
					rd_req 				<= '1';
				end if;
			end if;
			
			if 		USR_wr_func(address_offset	+	Slinkrocket_access_data) = '1' then
				wr_req 					<= '1';
			elsif  	USR_wr_func(address_offset	+	Slinkrocket_resync_request) = '1' then
				req_resync 				<= '1';
				wr_req 					<= '1';
			end if;	
		end if;
		
		if dt_valid_sync_user = '1' then
			USR_dt_rdy 				<= '1';
			access_end_reg 				<= '1';
		elsif USR_wen = '1' then
			if USR_wr_func(address_offset		+	Slinkrocket_access_command) = '1' and USR_dti(31) = '0' then
				USR_dt_rdy 			<= '0';
			end if;
			
			if (USR_wr_func(address_offset		+	Slinkrocket_access_command) = '1' and USR_dti(31) = '0') 	or 
				USR_wr_func(address_offset		+	Slinkrocket_access_data) = '1'								or
				USR_wr_func(address_offset		+	Slinkrocket_resync_request) = '1'							then
				access_end_reg 			<= '0';
			end if;
		end if;
	end if;
end process;

process(USR_resetn,USR_clk)
begin
    if USR_resetn = '0' then
        Cmd_OL      <= (others => '0');
        data_ol     <= (others => '0');
	elsif rising_edge(USR_clk) then
		if  USR_wen = '1' then
			if USR_wr_func(address_offset		+	Slinkrocket_access_command) = '1' then
				Cmd_OL 				<= USR_dti;
			end if;
			
			if USR_wr_func(address_offset		+	Slinkrocket_access_data) = '1' then
				data_ol 			<= USR_dti;
			end if;
		end if;	
	end if;
end process;


resync_pulse_pci_SXclk:entity work.resync_pulse 
port map (
	aresetn			=> USR_resetn,
	clocki			=> USR_clk,
	in_s			=> rd_req,
	clocko			=> clk_TX,
	out_s 			=> resync_rdrq
	);

resync_pulse_pci_SXclk_i2:entity work.resync_pulse 
port map (
	aresetn			=> USR_resetn,
	clocki			=> USR_clk,
	in_s			=> wr_req,
	clocko			=> clk_TX,
	out_s			=> resync_wrrq
	);
	
process(Grst_clk_TX,clk_TX)
begin
	if Grst_clk_TX = '0' then
		time_out 		<= (others => '0');
		set_TO			<= '0'; 
		rq_retrans 		<= '0';
	elsif rising_edge(clk_TX) then

		if resync_rdrq = '1' OR resync_wrrq = '1' OR rq_retrans = '1'  then 
			set_TO 		<= '1';								-- a Transmit Out is requested
			time_out 	<= timer_out;
		elsif inc_seqnm = '1' then							-- the seq number has been increased (ACK was received)
			set_TO 		<= '0';								--  release the Transmit Out request
		end if;
 
		rq_retrans 		<= '0';
		if set_TO = '1' AND time_out = x"0000" then			-- if a Transmit Out is requested and timeout is reached (countdown to 0) 
															-- => a retransmit is requested
			rq_retrans 	<= '1';
			set_TO 		<= '0';
		end if;
		
		if set_TO = '1' then								-- a Transmit Out is request ed timeout counter count down
			time_out	<= time_out - "1";
		end if;		
	end if;
end process;

	
COMMAND_req			<= '1' when resync_rdrq = '1' OR resync_wrrq = '1' OR rq_retrans = '1' else '0';
COMMAND_number		<= Cmd_OL;
COMMAND_data		<= data_ol;
COMMAND_Seq_num		<= seq_num;
access_end			<= access_end_reg;


-- mux the data returned to PCI bus
process(USR_clk)
begin
if rising_edge(USR_clk) then
	USR_dto_rg 			<= (others => '0');
	
    if USR_rd_func(address_offset			+	Slinkrocket_access_command) = '1' then
        USR_dto_rg(0) 	<= USR_dt_rdy;
        USR_dto_rg(1) 	<= access_end_reg;
        --USR_dto_rg(2) <= ;
    elsif USR_rd_func(address_offset		+	Slinkrocket_access_data) = '1' then
        USR_dto_rg 	    <= data_rcv;
    end if;

end if;
end process;

USR_dto	<= USR_dto_rg;

end behavioral;