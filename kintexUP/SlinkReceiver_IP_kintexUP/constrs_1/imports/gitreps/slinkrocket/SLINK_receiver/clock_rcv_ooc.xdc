create_clock -name usr_clk -period 10.0 [get_ports usr_clk ]
create_clock -name user_100MHz_clk -period 10.0 [get_ports clk_freerun_in ]
create_clock -name qpll_clkin -period 6.4 [ get_ports qpll_clkin ]
create_clock -name qpll_ref_clkin -period 6.4 [ get_ports qpll_ref_clkin ]
