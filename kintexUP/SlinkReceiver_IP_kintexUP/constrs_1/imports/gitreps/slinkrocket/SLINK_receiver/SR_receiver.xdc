
set_max_delay -datapath_only -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME=~ */*serdes_receiver_i1/txprbssel_async*[*]}]] -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME=~ */*serdes_receiver_i1/txprbssel_sync_inter*[*]}]] 3.000

set_max_delay -datapath_only -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME=~ */*serdes_receiver_i1/txprbssel_async*[*]}]] -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME=~ */*serdes_receiver_i1/rxprbssel_sync_rxclk*[*]*}]] 3.000

# TX clock SERDES
set_clock_groups -asynchronous -group [get_clocks -of_objects [get_pins -hierarchical -filter {NAME =~ */Receiver_core_i1/Manage_packet_build/start_framing*}]]
# RX clock SERDES
set_clock_groups -asynchronous -group [get_clocks -of_objects [get_pins -hierarchical -filter {NAME =~ */Receiver_core_i1/Manage_Packet_received/EndOfCRC*}]]

