# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "Add_Offset_SLINKR" -parent ${Page_0}
  ipgui::add_param $IPINST -name "Clock_source" -parent ${Page_0} -widget comboBox
  ipgui::add_param $IPINST -name "technology" -parent ${Page_0} -widget comboBox
  ipgui::add_param $IPINST -name "throughput" -parent ${Page_0} -widget comboBox
  ipgui::add_param $IPINST -name "UltraRam_mem" -parent ${Page_0}


}

proc update_PARAM_VALUE.Add_Offset_SLINKR { PARAM_VALUE.Add_Offset_SLINKR } {
	# Procedure called to update Add_Offset_SLINKR when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.Add_Offset_SLINKR { PARAM_VALUE.Add_Offset_SLINKR } {
	# Procedure called to validate Add_Offset_SLINKR
	return true
}

proc update_PARAM_VALUE.Clock_source { PARAM_VALUE.Clock_source } {
	# Procedure called to update Clock_source when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.Clock_source { PARAM_VALUE.Clock_source } {
	# Procedure called to validate Clock_source
	return true
}

proc update_PARAM_VALUE.UltraRam_mem { PARAM_VALUE.UltraRam_mem } {
	# Procedure called to update UltraRam_mem when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.UltraRam_mem { PARAM_VALUE.UltraRam_mem } {
	# Procedure called to validate UltraRam_mem
	return true
}

proc update_PARAM_VALUE.technology { PARAM_VALUE.technology } {
	# Procedure called to update technology when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.technology { PARAM_VALUE.technology } {
	# Procedure called to validate technology
	return true
}

proc update_PARAM_VALUE.throughput { PARAM_VALUE.throughput } {
	# Procedure called to update throughput when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.throughput { PARAM_VALUE.throughput } {
	# Procedure called to validate throughput
	return true
}


proc update_MODELPARAM_VALUE.Add_Offset_SLINKR { MODELPARAM_VALUE.Add_Offset_SLINKR PARAM_VALUE.Add_Offset_SLINKR } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.Add_Offset_SLINKR}] ${MODELPARAM_VALUE.Add_Offset_SLINKR}
}

proc update_MODELPARAM_VALUE.Clock_source { MODELPARAM_VALUE.Clock_source PARAM_VALUE.Clock_source } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.Clock_source}] ${MODELPARAM_VALUE.Clock_source}
}

proc update_MODELPARAM_VALUE.throughput { MODELPARAM_VALUE.throughput PARAM_VALUE.throughput } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.throughput}] ${MODELPARAM_VALUE.throughput}
}

proc update_MODELPARAM_VALUE.technology { MODELPARAM_VALUE.technology PARAM_VALUE.technology } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.technology}] ${MODELPARAM_VALUE.technology}
}

proc update_MODELPARAM_VALUE.UltraRam_mem { MODELPARAM_VALUE.UltraRam_mem PARAM_VALUE.UltraRam_mem } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.UltraRam_mem}] ${MODELPARAM_VALUE.UltraRam_mem}
}

