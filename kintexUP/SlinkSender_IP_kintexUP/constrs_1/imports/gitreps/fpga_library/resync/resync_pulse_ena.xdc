#set_max_delay -datapath_only -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ *resync_pulse*/reg_1st_stage*}]] -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME =~ *resync_pulse*/reg_2nd_stage_async*}]] 2.000
# Xilinx document says to set single bit CDC  to false path :https://support.xilinx.com/s/article/794116?language=en_US

set_false_path -from [get_pins -filter {REF_PIN_NAME == C} -of [get_cells -hierarchical -filter {NAME =~ *resync_pulse*/reg_1st_stage*}]] -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hierarchical -filter {NAME =~ *resync_pulse*/reg_2nd_stage_async*}]]

