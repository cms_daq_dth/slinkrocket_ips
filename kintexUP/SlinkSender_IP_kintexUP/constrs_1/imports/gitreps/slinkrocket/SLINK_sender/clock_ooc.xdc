create_clock -name user_100MHz_clk -period 10.0 [get_ports srds_freerunning_clock ]
create_clock -name qpll_clkin -period 6.4 [ get_ports qpll_clkin ]
create_clock -name qpll_ref_clkin -period 6.4 [ get_ports qpll_ref_clkin ]
